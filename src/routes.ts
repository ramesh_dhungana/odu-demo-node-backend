import express from 'express';
export const routes = express.Router();
import { firebaseAuth, firestoreDatabase } from './app';
import { v4 } from 'uuid';
import axios from 'axios';
import { isAuthenticated } from './middlewares';

routes.use([express.urlencoded({ extended: false }), express.json()]);

routes.post('/api/v1/auth/register', async (req, res) => {
	try {
		const { email, password, firstName, lastName, role, supervisor } = req.body;
		if (!email) {
			return res.status(500).json({ message: `Invalid email`, success: false });
		};
		if (!password) {
			return res.status(500).json({ message: `Invalid password`, success: false });
		}
		if (!firstName) {
			return res.status(500).json({ message: `Invalid firstName`, success: false });
		}
		if (!lastName) {
			return res.status(500).json({ message: `Invalid lastName`, success: false });
		}
		if (!role) {
			return res.status(500).json({ message: `Invalid role`, success: false });
		}
		try {
			const authUserRecord = await firebaseAuth
				.createUser({
					email: email,
					emailVerified: false,
					password: password,
					displayName: `${firstName}`,
					photoURL: `https://avatars.dicebear.com/api/jdenticon/${v4()}.svg`,
					disabled: false,
				});
			try {
				let userData: any = {
					firstName,
					lastName,
					roles: [role],
					email
				}
				if (supervisor) {
					userData = { ...userData, supervisor }
				}
				await firestoreDatabase.collection('users').doc(authUserRecord.uid).set(userData, { merge: true })
				return res.status(200).json({ message: `success`, success: true });
			} catch (error) {
				return res.status(500).json({ message: `Something went wrong :: ${error}`, success: false });
			}
		} catch (error) {
			return res.status(500).json({ message: `Something went wrong :: ${error}`, error, success: false });
		}

	} catch (error) {
		return res.status(500).json({ message: `Something went wrong :: ${error}`, error, success: false });
	}
});

routes.get('/api/v1/user', isAuthenticated, async (req, res) => {
	try {
		const userId = req.query.userId as string;
		if (!userId) {
			return res.status(422).json({ message: `Invalid role`, success: false });
		}
		try {
			try {
				const userSnap = await firestoreDatabase.collection('users').doc(userId).get();
				return res.status(200).json({ message: `success`, data: { ...userSnap.data(), id: userSnap.id }, success: true });
			} catch (error) {
				return res.status(500).json({ message: `Something went wrong :: ${error}`, success: false });
			}
		} catch (error) {
			return res.status(500).json({ message: `Something went wrong :: ${error}`, error, success: false });
		}

	} catch (error) {
		return res.status(500).json({ message: `Something went wrong :: ${error}`, error, success: false });
	}
});

routes.get('/api/v1/users', isAuthenticated, async (req, res) => {
	try {
		const { role } = req.query;
		const userUID = res.locals.authUserData.uid;
		try {
			try {
				let userQuery: FirebaseFirestore.Query<FirebaseFirestore.DocumentData>;
				if (role) {
					userQuery = firestoreDatabase.collection('users')
						.where('roles', 'array-contains', role)
					if (role === 'AGENT') {
						userQuery = userQuery.where('supervisor', '==', userUID);
					}
				} else {
					userQuery = firestoreDatabase.collection('users');
				}
				const userSnaps = await userQuery.get();
				let users = [];
				if (!userSnaps.empty) {
					userSnaps.forEach(snap => {
						users = [...users, { ...snap.data(), id: snap.id }]
					})
				}
				return res.status(200).json({ message: `success`, data: users, success: true });
			} catch (error) {
				return res.status(500).json({ message: `Something went wrong :: ${error}`, success: false });
			}
		} catch (error) {
			return res.status(500).json({ message: `Something went wrong :: ${error}`, error, success: false });
		}

	} catch (error) {
		return res.status(500).json({ message: `Something went wrong :: ${error}`, error, success: false });
	}
});

routes.post('/api/v1/auth/login', async (req, res) => {
	try {
		const { email, password } = req.body;
		if (!email) {
			return res.status(500).json({ message: `Invalid email`, success: false });
		};
		if (!password) {
			return res.status(500).json({ message: `Invalid password`, success: false });
		}
		console.log(email, password)
		const userRecordAxiosResponse = await axios.post(
			'https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword',
			{ email, password, returnSecureToken: true },
			{
				params: {
					key: 'AIzaSyDZ8fduTDerDtlDUknYzo2j2hObokJxe30',
				},
			}
		);
		const userRecord = userRecordAxiosResponse['data'];
		const userUID = userRecord.localId;
		const userSnap = await firestoreDatabase.collection('users').doc(userUID).get();
		console.dir(userUID, userSnap.data())
		const userData = { ...userSnap.data(), id: userUID, ...userRecord };
		return res.status(200).json({ message: `success`, success: true, data: userData });
	} catch (error) {
		console.log(error);
		return res.status(500).json({ message: `Something went wrong :: ${error}`, error, success: false });
	}
});


routes.get('/api/v1/mock/get', (req, res) => {
	try {
		return res.status(200).json({
			message: 'success',
		});
	} catch (error) {
		return res.status(500).json({ message: `Something went wrong :: ${error}`, success: false });
	}
});

routes.post('/api/v1/mock/post', (req, res) => {
	try {
		return res
			.status(200)
			.json({ message: 'Success.', score: 0.9, success: true, status: true });
	} catch (error) {
		return res.status(500).json({ message: `Something went wrong :: ${error}`, success: false });
	}
});


routes.post('/api/v1/add-message', isAuthenticated, async (req, res) => {
	try {
		const { author, text, to } = req.body;
		console.log(res.locals.authUserData);
		console.log(req.body);
		if (!author) {
			return res.status(500).json({ message: `Invalid author`, success: false });
		};
		if (!text) {
			return res.status(500).json({ message: `Invalid text`, success: false });
		}
		if (!to) {
			return res.status(500).json({ message: `Invalid to`, success: false });
		}
		const messageSnaps = await firestoreDatabase.collection('messages').get();
		try {
			const from = res.locals.authUserData.uid;

			await firestoreDatabase.collection('messages').add({
				from,
				to,
				timestamp: new Date(),
				author,
				selectionIndex: messageSnaps.size + 1,
				users: [to, from],
				reference: getMessageReference(from, to),
				text
			})
			return res.status(200).json({ message: `success`, success: true });
		} catch (error) {
			return res.status(500).json({ message: `Something went wrong:: ${error} `, error, success: false });
		}

	} catch (error) {
		return res.status(500).json({ message: `Something went wrong:: ${error} `, error, success: false });
	}
});

routes.get('/api/v1/messages', isAuthenticated, async (req, res) => {
	try {
		const to = req.query['to'] as string;
		const from = req.query['from'] as string;
		console.log('to, from ');
		console.log(to, from, req.query);
		if (!from) {
			return res.status(500).json({ message: `Invalid from`, success: false });
		}
		if (!to) {
			return res.status(500).json({ message: `Invalid to`, success: false });
		}
		const messages = await getMessages({ to, from });
		console.log('messages', messages)
		return res.status(200).json({ message: `success`, data: messages, success: true });
	} catch (error) {
		console.log(error);
		return res.status(500).json({ message: `Something went wrong:: ${error} `, error, success: false });
	}
});

async function getMessages(params: { to: string, from: string }) {
	const { to, from } = params;
	let messageSnaps: FirebaseFirestore.QuerySnapshot<FirebaseFirestore.DocumentData>;
	const reference = getMessageReference(from, to);
	messageSnaps = await firestoreDatabase.collection('messages')
		.where('reference', '==', reference)
		.orderBy('timestamp', 'asc')
		.get();
	let messages = [];
	if (!messageSnaps.empty) {
		messageSnaps.forEach(snap => {
			const data = snap.data();
			messages = [...messages, { ...snap.data(), timestamp: data.timestamp.toDate() }]
		})
	}
	return messages;
}

function getMessageReference(from: string, to: string) {
	const sortedUsers = [from, to].sort();
	return `${sortedUsers[0]}_${sortedUsers[1]}`;
}