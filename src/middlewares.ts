import { Request, Response } from 'express';
import { firebaseAuth } from './app';

export async function isAuthenticated(req: Request, res: Response, next: Function) {
	const { authorization } = req.headers;

	if (!authorization) {
		console.log('Unauthorized: No Authorization Header');
		return res.status(401).send({ success: false, message: 'Unauthorized' });
	}

	if (!authorization.startsWith('Bearer')) {
		console.log('Unauthorized: No Bearer Token');
		return res.status(401).send({ success: false, message: 'Unauthorized' });
	}

	const split = authorization.split('Bearer ');
	if (split.length !== 2) {
		console.log('Unauthorized: Invalid Authorization Header Format');
		return res.status(401).send({ success: false, message: 'Unauthorized' });
	}

	const token = split[1];

	try {
		const decodedToken = await firebaseAuth.verifyIdToken(token);
		console.log('Decoded Token', JSON.stringify(decodedToken));
		const authUserData = {
			uid: decodedToken.uid,
			email: decodedToken.email,
			displayName: decodedToken.name,
		};
		res.locals = { ...res.locals, authUserData };
		return next();
	} catch (err) {
		console.error(`${err.code} -  ${err.message}`);
		return res.status(401).send({ success: false, message: 'Unauthorized' });
	}
}


