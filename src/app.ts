require('dotenv').config();
if (process.env.ENVIRONMENT != 'LOCAL') require('@google-cloud/debug-agent').start({ serviceContext: { enableCanary: true } });
import cors from 'cors';
import path from 'path';
import admin from 'firebase-admin';
import express from 'express';
import { routes } from './routes';

const PORT = Number(process.env.PORT) || 8080;
const serviceAccountFilePath = path.resolve(__dirname, '..') + '/config/firebase/config.json';
const serviceAccount = require(serviceAccountFilePath);

admin.initializeApp({
	credential: admin.credential.cert(serviceAccount),
});
export const firestoreDatabase = admin.firestore();
export const firebaseAuth = admin.auth();

const app = express();
app.use(cors());
app.use((req, res, next) => {
	res.header('Access-Control-Allow-Origin', '*');
	res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept');
	res.header('Access-Control-Allow-Methods', 'OPTIONS, GET, POST, PUT, DELETE');
	console.log(
		`[${new Date().toDateString()} - ${new Date().toLocaleTimeString('en-US', {
			timeZone: 'America/New_York',
			timeZoneName: 'short',
		})}]: ${req.method} request received from IP: ${req.ip} for endpoint: ${req.protocol}://${req.get('Host')}${req.originalUrl}`
	);
	if (req.method === 'OPTIONS') {
		res.sendStatus(200);
	} else {
		next();
	}
});

// Handle POST requests that come in formatted as JSON.
app.use(express.json());

// Ensuring we get the correct client IP and not the IP of a proxy server, load balancer or similar resource.
app.set('trust proxy', true);

app.use('/', [routes]);
app.use('/api/v1/mock/get', routes);
app.use('/api/v1/mock/post', routes);
app.use('/api/v1/auth/login', routes);
app.use('/api/v1/auth/register', routes);
app.use('/api/v1/user/', routes);
app.use('/api/v1/add-message/', routes);
app.use('/api/v1/messages/', routes);

app.listen(PORT, () => {
	return console.log(`Express is listening at http://localhost:${PORT}`);
});

